file = open("caterpillar.js", "r")
### Make one big string with the file
list = "".join(file.readlines())
### Split the file for each "&&" operators, because it split correctly each equalities tests
list = list.split("&&")
dictionaire = {}
### for each equality test
for i in list:
    ### split the two equalities values
    l=i.split("==")
    ### index is always at left, and value always at right
    dictionaire[l[0].count("-~")] = chr(l[1].count("-~"))
### sort by index
d=sorted(dictionaire.items(), key=lambda t: t[0])
### retrieve flag
print("".join( j for i,j in d) )