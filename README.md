# caterpillar               200 pts


## Description

-~-~-~-~[]? -~-~-~-~[].

file : caterpillar.js

## Analyse

We have a js script with very weird sentence. we can see for each line, the script check if the x th value is equal to the corresponding flag char.

So to retrieve the flag, we just have to retrieve the values and sort them with there index.

## Methode

First, we gonna replace the weird sentence. Because "-~-~-~-~[]" is interpreted by the js console as the 4, the number of "~-", we just have to replace each of these sentence by there represented number.

Do this manually is not a good idea because it take time and may cause errors, so i write a litle script that read the file, retrieve and stock indexs and values of the flag, sort the dict and return the flag. 

